// ENFORCE HTTPS
if (location.protocol != 'https:') location.href = 'https:' + window.location.href.substring(window.location.protocol.length);

function escattr ( s ) {
	return s.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(/"/g,'&quot;').replace(/'/g,'&#x27;').replace(/\//g,'&#x2F;') ;
}

function myURIescape ( s ) {
	return encodeURIComponent(s).replace(/'/g, "&#39;") ;
}


Vue.component ( 'default-header' , {
	template : '#default-header-template'
} ) ;


Vue.component ( 'upload-button' , {
	props : [ 'q' ] ,
	mounted : function () { wlmuk.tt.updateInterface(this.$el) } ,
	methods : {
		doUpload : function () {
			var me = this ;
			var url = wlmuk.getUploadURL({q:me.q}) ;
			window.open(url, '_blank');
			wlmuk.watchItem(me.q);
//			wlmuk.uploadFile ( me.q ) ;
		} ,
	} ,
	template : '#upload-button-template'
} ) ;


Vue.component ( 'search-location' , {
	data : function () { return { query:'' , results:[] , message:'' } } ,
//	created : function () { tt.updateInterface(this.$el) } ,
	updated : function () { wlmuk.tt.updateInterface(this.$el) } ,
	mounted : function () { wlmuk.tt.updateInterface(this.$el) } ,
	methods : {
		search : function () {
			var me = this ;
			me.results = [] ;
			me.message = wlmuk.tt.t('searching') ;
			$.getJSON ( 'https://www.wikidata.org/w/api.php?callback=?' , {
				action:'wbsearchentities',
				search:me.query,
				limit:50,
				format:'json',
				language:'en' // TODO
			} , function ( d ) {
				var qs = [] ;
				$.each ( d.search , function ( dummy , res ) {
					qs.push ( res.id ) ;
				} ) ;
				
				if ( qs.length == 0 ) {
					me.message = wlmuk.tt.t('alert_no_results') ;
					return ;
				}
				
				me.message = '' ;
				var sparql = '' ;
				sparql = 'SELECT DISTINCT ?q ?qLabel ?desc ?coordinates {' ;
				sparql += ' VALUES ?q { wd:' + qs.join(' wd:') + '}' ;
				sparql += '  ?q wdt:P31/wdt:P279* wd:Q486972 . OPTIONAL { ?q wdt:P131* ?liae } FILTER ( ?liae = wd:'+ wlmuk.country_data.admin_region + ' ) OPTIONAL { ?q schema:description ?desc } FILTER ( lang(?desc)="en" ) SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }' ;
				sparql += ' ?q wdt:P625 ?coordinates' ;
				sparql += ' }' ;

				$.get ( wlmuk.sparql_url , {
					query : sparql
				} , function ( d ) {
					var new_results = {} ;
					me.result_order = [] ;
					$.each ( d.results.bindings , function ( dummy , b ) {
						var r = {
							q:b.q.value.replace(/^.+\/Q/,'Q'),
							label:b.qLabel.value,
							desc:''
						} ;

						r.pos = b.coordinates.value.replace(/^Point\(/,'').replace(/\)$/,'').split(' ') ;
						r.pos = { lon:r.pos[0] , lat:r.pos[1] } ;
						
						if ( typeof b.desc != 'undefined' ) r.desc = b.desc.value ;
						me.results.push ( r ) ;
					} ) ;
					if ( me.results.length == 0 ) {
						me.message = wlmuk.tt.t('alert_no_results') ;
					} else {
						wlmuk.is_nearby_location = false ;
					}
				} , 'json' ) ;


				
			} ) ;
			return false ;
		} ,
		goto_gps : function ( r ) {
//		console.log ( r ) ;
			wlmuk.force_update_gps = true ;
//			wlmuk.router.push('/gps/'+r.pos.lat+','+r.pos.lon);
			wlmuk.router.push('/q/'+r.q);
		} ,
	} ,
	template : '#search-location-template'
} ) ;



Vue.component ( 'nearby-button' , {
//	created : function () { tt.updateInterface(this.$el) } ,
//	updated : function () { tt.updateInterface(this.$el) } ,
	mounted : function () { wlmuk.tt.updateInterface(this.$el) } ,
	methods : {
		nearby : function () {

/*		
			function switchToGPS ( pos ) {
				console.log ( pos ) ;
			}
		
			if ( "geolocation" in navigator ) {
				navigator.geolocation.getCurrentPosition ( switchToGPS , function (e) { console.log('error',e)} ) ;
			} else {
				alert ( "Could not determine your current location, sorry!" ) ;
			}
*/

			function auto_coords ( pos ) {
				wlmuk.is_nearby_location = true ;
				wlmuk.force_update_gps = true ;
				wlmuk.router.push('/gps/'+pos.coords.latitude+','+pos.coords.longitude);
			}
	
			function no_auto_coords () {
				alert ( "Could not determine your current location, sorry!" ) ;
			}

			if(geo_position_js.init()){
				geo_position_js.getCurrentPosition(auto_coords,no_auto_coords);
			}

		} ,
	} ,
	template : '#nearby-button-template'
} ) ;


var the_map = {};

Vue.component ( 'leafmap' , {
	props : [ 'lat' , 'lon' , 'start_zoom' , 'latlon' ] ,
	data : function () { return { zoom_level:14 , map:{} , map_is_set:false , markers:{} , loading:false , redMarker:{} , blueMarker:{} , yellowMarker:{} , list:[] , zoom_level_message:'' , green_message:'' } } ,
	mounted : function () {
		var me = this ;
		the_map = me ;
		me.createMap() ;
	} ,
	updates : function () {
		//console.log ( lat , lon , zoom ) ;
	} ,
	methods : {
		createMap : function () {
		
			var me = this ;
			if ( me.map_is_set ) return ;

			wlmuk.tt.updateInterface () ;

			me.greenMarker = L.AwesomeMarkers.icon ( { markerColor:'green' } ) ; // , iconSize: [30,30]
			me.redMarker = L.AwesomeMarkers.icon ( { markerColor:'red' } ) ;
			me.yellowMarker = L.AwesomeMarkers.icon ( { markerColor:'orange' } ) ;
			me.blueMarker = L.AwesomeMarkers.icon ( { markerColor:'blue' } ) ; // , icon:'fa-camera-retro' , iconColor:'white'

			if ( typeof me.start_zoom != 'undefined' ) me.zoom_level = me.start_zoom * 1 ;
			me.map_is_set = true ;
			me.map = L.map('map', {
			}).setView ( [ me.lat , me.lon ] , me.zoom_level ) ;
			L.tileLayer('https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png', {
				attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
			}).addTo(me.map);
			me.map.on ( 'viewreset' , function () { me.mapChanged() } ) ;
			me.map.on ( 'zoomend' , function () {
				var z = me.map.getZoom() ;
				if ( z <= 11 ) return ; // Max zoom level
				if ( z > me.zoom_level ) {
					me.zoom_level = z ; // No need to reload data

					var center = me.map.getCenter() ;
					var url = '/gps/' + center.lat + ',' + center.lng ;
					if ( typeof zoom != 'undefined' && zoom != 14 ) url += '/' + z ;
//					wlmuk.router.replace ( url ) ;
					me.updateMarkers() ;

				} else {
					me.zoom_level = z ;
					me.mapChanged() ;
				}
			} ) ;
			me.map.on ( 'dragend' , function () { me.mapChanged() } ) ;


			me.markers['me'] = L.marker([me.lat, me.lon],{icon:me.greenMarker}) ;
			me.markers['me'].addTo(me.map).bindPopup("You") ;

			if ( wlmuk.is_nearby_location ) {
//				console.log ( "NEARBY ON" ) ;
				if ( typeof wlmuk.position_watcher != 'undefined' ) navigator.geolocation.clearWatch ( wlmuk.position_watcher ) ;
				wlmuk.position_watcher = navigator.geolocation.watchPosition(function(position) {
					var p = me.gps2leaflet(position.coords) ;
//					console.log ( 'UPDATING POSITION' , position , p ) ;
					me.markers['me'].setLatLng ( [ p.lat , p.lng ] ) ;
				} ) ;
				me.green_message = wlmuk.tt.t ( 'your_location' ) ;
			} else {
				me.green_message = '' ;
			}

			me.mapChanged () ;
		} ,
		
		gps2leaflet : function ( gps ) {
			return { lat:gps.latitude , lng:gps.longitude } ;
		} ,
		
		reCenter : function ( lat , lon , zoom ) {
			var me = this ;
			me.map.setView ( [ lat , lon ] , zoom ) ;
			me.mapChanged() ;
		} ,

		
		mapChanged : function () {
			var me = this ;
			me.markers = {} ; // Force current
			var b = me.map.getBounds() ;
			var ne = b.getNorthEast() ;
			var sw = b.getSouthWest() ;
			me.loading = true ;
			$('#map').addClass('disabled') ;
			wlmuk.getItemList ( ne , sw , me.map.getZoom() , me.updateMarkers ) ;
		} ,
		
		updateMarkers : function () {
			var me = this ;
			
			me.zoom_level_message = '' ;
			if ( me.zoom_level < wlmuk.no_markers_zoom_threshold ) {
				$.each ( me.markers , function ( q , marker ) {
					if ( !q.match(/^Q\d+$/) ) return ;
					me.map.removeLayer ( marker ) ;
					delete me.markers[q] ;
				} ) ;
				me.zoom_level_message = wlmuk.tt.t('zoom_in_for_pins') ;
			}
			
			var list = [] ;
			$.each ( wlmuk.result_order , function ( num , q ) {
				var o = wlmuk.results[q] ;
				
				var li = { q:q , label:o.label } ;
				if ( typeof o.img != 'undefined' ) {
					li.img = o.img ;
					li.thumb_url = "https://commons.wikimedia.org/wiki/Special:Redirect/file/"+escattr(encodeURIComponent(o.img))+'?width=128px&height=32px' ;
				}
				if ( typeof o.heritage_designation != 'undefined' ) li.heritage_designation = o.heritage_designation ;
				list.push ( li ) ;
				
				if ( typeof me.markers[q] != 'undefined' ) return ; // Already have that marker
				var html = '' ;
				html += "<div><a target='_blank' href='https://www.wikidata.org/wiki/" + q + "'>" + o.label + "</a></div>" ;
				if ( typeof o.img != 'undefined' ) {
					var img_url = "https://commons.wikimedia.org/wiki/Special:Redirect/file/"+escattr(encodeURIComponent(o.img))+'?width='+wlmuk.thumb_size+'px&height='+wlmuk.thumb_size+'px' ;
					html += "<div class='card text-center' style='width:10rem'>" ;
					html += "<img class='card-img-top' src='" + img_url + "' />" ;
					html += "</div>" ;
				}
				
				if ( typeof o.heritage_designation != 'undefined' ) {
					html += "<div>" ;
					html += o.heritage_designation ;
					html += "</div>" ;
				}
				
				html += "<div style='text-align:center'>" ;
				html += wlmuk.getUploadLink ( { q:q } ) ;
				html += "</div>" ;
				let the_marker = me.redMarker;
				if (o.currently_uploading) the_marker = me.yellowMarker;
				else if (typeof o.img != 'undefined') the_marker = me.blueMarker ;
				me.markers[q] = L.marker([o.pos.lat, o.pos.lon],{icon:the_marker}) ;
				me.markers[q].addTo(me.map).bindPopup(html) ;
				if ( !wlmuk.mobile ) { // Desktop
					var start ;
					me.markers[q].on('mouseover', function (e) {
						this.openPopup();
						start = new Date().getTime();
					});
					me.markers[q].on('mouseout', function (e) {
						var end = new Date().getTime();
						var time = end - start;
						if(time > 1700) this.closePopup();  // Just making up a number...
					});
				}
			} ) ;
			
			$('#map').removeClass('disabled') ;
			me.list = list ;
			me.loading = false ;
		}
	} ,
	watch : {
		latlon : function (a,b) { console.log ( "A" , a , b ) ; } , //this.reCenter(b,this.lon,this.zoom_level) } ,
//		"wlmuk.just_set_image" : {handler:function(){this.mapChanged()},deep:true},
//		lat : function (a,b) { console.log ( "A" ) ; this.reCenter(b,this.lon,this.zoom_level) } ,
//		lon : function (a,b) { console.log ( "B" ) ; this.reCenter(this.lat,b,this.zoom_level) } ,
//		zoom : function (a,b) { console.log ( "C" ) ; this.reCenter(this.lat,this.lon,b) } ,
	} ,
	template: '#leafmap-template'
} ) ;





var MainPage = Vue.extend ( {
	template: '#main-page-template'
} ) ;



var WikidataItem = Vue.extend ( {
	props : [ 'q' ] ,
	data : function () { return { lat:0 , lon:0 , has_coords:false , zoom:14 , latlon:'' } } ,
	created : function () {
		this.init ( this.q ) ;
	} ,
	methods : {
		init : function ( _q ) {
			var me = this ;
			me.has_coords = false ;
			var q = 'Q'+(''+_q).replace(/\D/g,'') ;
			wlmuk.wd.getItemBatch ( [q] , function () {
				var i = wlmuk.wd.getItem ( q ) ;
				if ( typeof i == 'undefined' ) {
					alert ( "No auch item " + q ) ;
					return ;
				}
				var claims = i.getClaimsForProperty ( 'P625' ) ;
				if ( claims.length == 0 ) {
					alert ( "Item " + q + " has no coordinates" ) ;
					return ;
				}
				var pos = claims[0].mainsnak.datavalue.value ;
				me.lat = pos.latitude*1 ;
				me.lon = pos.longitude*1 ;
				me.latlon = me.lat+','+me.lon ;
				me.has_coords = true ;
			} ) ;
		} ,
	} ,
	watch : {
		q : function ( _old , _new ) {
			this.init ( _new ) ;
			location.reload() ;
		} ,
	} ,
	template : '#wikidata-item-template'
} ) ;


var GPS = Vue.extend ( {
	props : [ 'coords' , 'start_zoom' ] ,
	data : function () { return { lat:0 , lon:0 , has_coords:false , zoom:14 } } ,
	created : function () {
		var me = this ;
		me.init ( me.coords , me.start_zoom ) ;
	} ,
	methods : {
		init : function ( coords , zoom ) {
			var me = this ;
//console.log ( "INIT" , me.lat , me.lon , coords , zoom ) ;
			if ( typeof zoom != 'undefined' ) me.zoom = zoom * 1 ;
			if ( typeof coords != 'undefined' && coords != '' ) {
				var parts = coords.split ( ',' ) ;
				me.lat = parts[0] * 1 ;
				me.lon = parts[1] * 1 ;
				me.has_coords = true ;
			}
			wlmuk.force_update_gps = false ;
		} ,
	} ,
	watch : {
		coords : function ( e , f ) {
//			console.log ( "!" , e , f ) ;
			if ( wlmuk.force_update_gps ) this.init ( f ) ;
		} ,
	} ,
	template: '#gps-template'
} ) ;



var wlmuk = {
	mobile : false ,
	no_markers_zoom_threshold : 13 ,
	is_nearby_location : false , 
	force_update_gps : false , 
	sparql_url : 'https://query.wikidata.org/bigdata/namespace/wdq/sparql' ,
	country_data : {
		heritage_props : [ 'P709','P718','P1216','P1459','P1460','P3007' ] ,
		prop2campaign : {
			'P1459':{campaign:'wlm-gb-wls',cats:['Images from Wiki Loves Monuments $YEAR in Wales']},
			'P3007':{campaign:'wlm-gb-wls',cats:['Images from Wiki Loves Monuments $YEAR in Wales']},
			'P1460':{campaign:'wlm-gb-nir',cats:['Images from Wiki Loves Monuments $YEAR in Northern Ireland']},
			'P1216':{campaign:'wlm-gb-eng',cats:['Images from Wiki Loves Monuments $YEAR in England']},
			'P709':{campaign:'wlm-gb-sct',cats:['Images from Wiki Loves Monuments $YEAR in Scotland']},
			'P718':{campaign:'wlm-gb-sct',cats:['Images from Wiki Loves Monuments $YEAR in Scotland']},
		} ,
		date_category : 'WLM-UK $YEAR unfiltered $MONTH-$DAY' ,
		admin_region : 'Q145' ,
		cats : [ 'Images from Wiki Loves Monuments $YEAR in the United Kingdom' ] ,
		language : 'en'
	} ,
	thumb_size : '120px' ,

	is_logged_in : false ,
	user_name : '' ,
	results : {} ,
	result_order : [] ,
	tt : {} ,
	wd : {} ,
	router : {} ,
	app : {} ,
	routes : [
		{ path: '/', component: MainPage },
		{ path: '/gps/:coords', component: GPS , props:true } ,
		{ path: '/gps/:coords/:start_zoom', component: GPS , props:true } ,
		{ path: '/q/:q', component: WikidataItem , props:true } ,
	] ,
	item_watchers: {},
	just_set_image:{},


	// See https://commons.wikimedia.org/wiki/Commons:Wiki_Loves_Monuments_2016/Upload_campaigns#How_to_link_to_the_upload_campaign_and_pre-fill_it.3F
	getUploadURL : function ( v ) {
		var me = this ;
		var i = me.results[v.q] ;
		if ( typeof i == 'undefined' ) return '' ;
		
		var categories = [] ;

		var url = 'https://commons.wikimedia.org/w/index.php?title=Special:UploadWizard' ;
		url += '&lat=' + i.pos.lat ;
		url += '&lon=' + i.pos.lon ;
		$.each ( me.country_data.prop2campaign , function ( property , camp ) {
			if ( typeof i.heritage_data[property] == 'undefined' ) return ;
			url += '&campaign=' + camp.campaign ;
			url += '&id=' + i.heritage_data[property] ;
			if ( typeof camp.cats != 'undefined' ) {
				$.each ( camp.cats , function ( dummy , category ) {
					category = category.replace(/\$YEAR/,year).replace(/\$MONTH/,month).replace(/\$DAY/,day) ;
//					categories.push ( category ) ; // Deactivated on request
				} ) ;
			}
			return false ; // Only one
		} ) ;
		
		$.each ( (me.country_data.cats||[]) , function ( dummy , category ) {
			category = category.replace(/\$YEAR/,year).replace(/\$MONTH/,month).replace(/\$DAY/,day) ;
//			categories.push ( category ) ;  // Deactivated on request
		} ) ;
//		if ( typeof i.commons_category != 'undefined' ) categories.push ( encodeURIComponent(i.commons_category) ) ; // Deactivated on request
		if ( typeof i.img == 'undefined' ) categories.push ( 'Potential image for Wikidata item' ) ;
		
		if ( typeof me.country_data.date_category != 'undefined' ) {
			var date = new Date() ;
			var year = date.getFullYear();
			var month = date.getMonth()+1;
			month = (month>9?'':'0')+month;
			var day = date.getDate()+1;
			day = (day>9?'':'0')+day ;
			var datecat = me.country_data.date_category.replace(/\$YEAR/,year).replace(/\$MONTH/,month).replace(/\$DAY/,day) ;
//			categories.push ( datecat ) ;  // Deactivated on request
		}

		var desc = [] ;
		desc.push ( i.label ) ;
		desc.push ( '{{On Wikidata|' + v.q + '}}' ) ;


		if ( categories.length > 0 ) url += '&categories=' + categories.join('|') ;
		url += '&description=' + encodeURIComponent ( desc.join("<br/>\n") ) ;
		url += "&descriptionlang=en" ; //+ ( me.country_data.language || 'en' ) ;
		url += "&uselang=" + ( me.country_data.language || 'en' ) ;

		return url ;
	} ,
	
	getUploadLink : function ( v ) {
		var me = this ;
		var url = me.getUploadURL ( v ) ;
		var h = '' ;
		h += " <a href='" + escattr(url) + "' target='_blank' class='btn btn-outline-primary' tt='upload' onclick='wlmuk.watchItem(\""+v.q+"\")'>" ;
		h += wlmuk.tt.t ( 'upload' ) ;
		h += "</a>" ;
		return h ;
	} ,

	
	getItemList : function ( ne , sw , zoom , callback ) {
		var me = this ;
		var sparql = '' ;
		sparql += 'SELECT ?q ?qLabel ?coord ?img ?commons_category ?hdLabel' ;
		$.each ( me.country_data.heritage_props , function ( dummy , p ) {
			sparql += " ?" + p ;
		} ) ;
		sparql += ' {' ;
		sparql += ' VALUES ?prop { wdt:' + me.country_data.heritage_props.join(' wdt:') + ' } ?q ?prop []' ;
		sparql += ' OPTIONAL { ?q wdt:P373 ?commons_category }' ;
		sparql += ' OPTIONAL { ?q wdt:P1435 ?hd }' ;
		$.each ( me.country_data.heritage_props , function ( dummy , p ) {
			sparql += ' OPTIONAL { ?q wdt:' + p + ' ?' + p + '} ' ;
		} ) ;
		sparql += ' SERVICE wikibase:box { ?q wdt:P625 ?location . ' ;
		sparql += 'bd:serviceParam wikibase:cornerSouthWest "Point('+sw.lng+' '+sw.lat+')"^^geo:wktLiteral . ' ;
		sparql += 'bd:serviceParam wikibase:cornerNorthEast "Point('+ne.lng+' '+ne.lat+')"^^geo:wktLiteral }' ;
		sparql += ' ?q wdt:P625 ?coord . SERVICE wikibase:label { bd:serviceParam wikibase:language "en". } OPTIONAL { ?q wdt:P18 ?img } }' ;
		sparql += ' LIMIT 500' ;
		
		var center = { lat:(ne.lat+sw.lat)/2 , lon:(ne.lng+sw.lng)/2 } ; // Yes I know...
/*		var url = '/gps/' + center.lat + ',' + center.lon ;
		if ( typeof zoom != 'undefined' && zoom != 14 ) url += '/' + zoom ;
		me.router.replace ( url ) ;*/
		
		if ( zoom < me.no_markers_zoom_threshold ) {
			me.results = {} ;
			me.result_order = [] ;
			if ( typeof callback != 'undefined' ) callback() ;
			return ;
		}

		$.get ( me.sparql_url , {
			query : sparql
		} , function ( d ) {
			var new_results = {} ;
			me.result_order = [] ;
			$.each ( d.results.bindings , function ( dummy , b ) {
				var q = b.q.value.replace ( /^.+\/entity\// , '' ) ;
				if ( typeof new_results[q] != 'undefined' ) return ;
				var o = { q:q , label:q , heritage_data:{} } ;
				$.each ( me.country_data.heritage_props , function ( dummy , p ) {
					if ( typeof b[p] == 'undefined' ) return ;
					if ( b[p].type != 'literal' ) return ;
					o.heritage_data[p] = b[p].value ;
				} ) ;
				
				if ( typeof b.commons_category != 'undefined' && b.commons_category.type == 'literal' ) o.commons_category = b.commons_category.value ;
				if ( typeof b.hdLabel != 'undefined' && b.hdLabel.type == 'literal' ) o.heritage_designation = b.hdLabel.value ;
				if ( typeof b.qLabel != 'undefined' && b.qLabel.type == 'literal' ) o.label = b.qLabel.value ;
				if ( typeof b.img != 'undefined' && b.img.type == 'uri' ) o.img = unescape ( b.img.value.replace ( /^.+\/Special:FilePath\// , '' ) ) ;
				if ( typeof b.coord != 'undefined' && b.coord.type == 'literal' ) {
					o.pos = b.coord.value.replace(/^Point\(/,'').replace(/\)$/,'').split(' ') ;
					o.pos = { lon:o.pos[0] , lat:o.pos[1] } ;
					o.distance = ((o.pos.lon-center.lon)*(o.pos.lon-center.lon)+(o.pos.lat-center.lat)*(o.pos.lat-center.lat)) ; // Just for sort order
				}
				if ( typeof o.img == 'undefined' && typeof me.just_set_image[q] != 'undefined' ) {
					if ( typeof me.just_set_image[q].img != 'undefined' ) o.img = me.just_set_image[q].img ;
					else o.currently_uploading = true ;
				}
				new_results[q] = o ;
				me.result_order.push ( q ) ;
			} ) ;
			me.result_order.sort ( function ( a , b ) {
				return new_results[a].distance-new_results[b].distance ;
			} ) ;
			me.results = new_results ;
			if ( typeof callback != 'undefined' ) callback() ;
		} , 'json' ) ;

	} ,

	watchItem : function ( q ) {
		let me = this ;
		Vue.set(me.just_set_image,q, {uploading:true}) ;
		delete the_map.markers[q];
		the_map.mapChanged();
		me.item_watchers[q] =  setInterval(function(){
			//console.log("Checking item "+q);
			$.getJSON("https://www.wikidata.org/w/api.php?callback=?&action=wbgetentities&format=json&ids="+q,function(d){
				if (typeof d.entities[q].claims.P18 == 'undefined' ) return;
				let image = d.entities[q].claims.P18[0].mainsnak.datavalue.value ;
				//console.log("Has image '"+image+"'!");
				Vue.set(me.just_set_image,q, {img:image}) ;
				clearInterval(me.item_watchers[q]);
				delete the_map.markers[q];
				the_map.mapChanged();
			});
		},1000);
	} ,
	
	uploadFile : function ( q ) {
		var me = this ;

/*
=={{int:filedesc}}==
{{Information
|description={{Listed building England|1079910}}
|date=2016-09-26 17:53:25
|source={{own}}
|author=~~~
|permission=
|other versions=
}}
{{self|cc-by-sa-4.0}}
{{Wiki Loves Monuments 2018|gb}}
[[Category:Images from Wiki Loves Monuments 2018 in the United Kingdom]]
[[Category:WLM-UK 2018 unfiltered 09-27]] ???
[[Category:Uploaded via Campaign:wlm-gb-eng]]
[[Category:Photographs by ~~~]]
*/
	} ,

	checkAuth : function ( callback ) {
		var me = this ;
		$.post ( './api_vue.php' , {
			action:'check'
		} , function ( d ) {
			if ( typeof d.result.error != 'undefined' ) {
				$('div.login').show() ;
			} else {
				me.is_logged_in = true ;
				me.user_name = d.result.query.userinfo.name ;
			}
			if ( typeof callback != 'undefined' ) callback() ;
		} , 'json' ) ;
	} ,
	
	authClicked : function () {
		var me = wlmuk ;
		if ( me.is_logged_in ) {
			$('div.login').hide() ;
			return ;
		}
		setTimeout ( function () {
			me.checkAuth ( me.authClicked ) ;
		} , 1000 ) ;
	} ,

	init : function () {
		var me = this ;
		wlmuk.mobile = ( ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch ) ? true : false ;

		var cnt = 2 ;
		function cb () {
			cnt-- ;
			if ( cnt > 0 ) return ;
			me.tt.addILdropdown ( '#tooltranslate_wrapper' ) ;
			me.wd = new WikiData ;
			me.tt.updateInterface () ;

			me.router = new VueRouter({routes:me.routes}) ;
			me.app = new Vue ( { router:me.router } ) .$mount('#app') ;
		}
		
		me.checkAuth(cb) ;
		me.tt = new ToolTranslation ( {
			tool : 'wlm_wd' ,
			language : 'en' ,
			fallback : 'en' ,
			highlight_missing : true ,
			onUpdateInterface : function () {
			} ,
			callback : cb
		} ) ;
	} ,


} ;


$(document).ready ( function () {
	wlmuk.init() ;
} ) ;
