#!/usr/bin/php
<?PHP

require_once ( 'php/common.php' ) ;

$sets = array (
	array ( 'campaign'=>'nir' , 'prop'=>1460 , 'type'=>'building' , 'use_id_column'=>'hbnum' , 'id_pattern'=>'/^HB.+$/' , 'source_item'=>'Q936287' , 'grades'=>array('A'=>'','B+'=>'') ) ,
	array ( 'campaign'=>'sct' , 'prop'=>709 , 'type'=>'building' , 'use_id_column'=>'hbnum' , 'id_pattern'=>'/^\d+$/' , 'source_item'=>'Q111854' , 'grades'=>array('A'=>'Q10729054','B'=>'Q10729125') ) ,
	array ( 'campaign'=>'eng' , 'prop'=>1216 , 'type'=>'building' , 'use_id_column'=>'ext_id' , 'id_pattern'=>'/^\d+$/' , 'source_item'=>'Q936287' , 'grades'=>array('I'=>'Q15700818','II*'=>'Q15700831') ) ,
	array ( 'campaign'=>'wls' , 'prop'=>1459 , 'type'=>'building' , 'use_id_column'=>'ext_id' , 'id_pattern'=>'/^\d+$/' , 'source_item'=>'Q936287' , 'grades'=>array('I'=>'Q15700818','II*'=>'Q15700831') ) ,
	array ( 'campaign'=>'eng' , 'prop'=>1216 , 'type'=>'monument' , 'use_id_column'=>'ext_id' , 'id_pattern'=>'/^\d+$/' , 'source_item'=>'Q936287' ) ,
) ;


$db = openToolDB ( 'wlmuk_p' ) ;
$dbwd = openDB ( 'wikidata' , '' ) ;

function createCoordinates ( $data ) {
	global $db , $dbwd , $wdq_internal_url ;
	$campaign = $data['campaign'] ;
	$use_id_column = $data['use_id_column'] ;
	$main_prop = $data['prop'] ;
	$type = $data['type'] ;
	$source_item = $data['source_item'] ;
	$has_main_prop = array() ;

	// Get existing coordinates
	$url = "$wdq_internal_url?q=claim[$main_prop]" ; // &props=$main_prop,625
	$j = file_get_contents ( $url ) ;
	$j = json_decode ( $j ) ;


	$q_no_coord = array() ;
	$sql = "SELECT distinct epp_entity_id FROM wb_entity_per_page WHERE epp_entity_type='item' AND epp_entity_id IN (" . implode(',',$j->items) . ") AND NOT EXISTS ( SELECT * FROM pagelinks where pl_from=epp_page_id  and pl_namespace=120 and pl_title='P625' and pl_from_namespace=0)" ;
	if(!$result = $dbwd->query($sql)) die('There was an error running the query [' . $dbwd->error . ']: '."\n$sql\n");
	while($o = $result->fetch_object()){
		$q_no_coord[$o->epp_entity_id] = 1 ;
	}

	if ( $source_item == '' ) $source = "\n" ;
	else $source = "\tS143\t$source_item\n" ;
	$sql = "SELECT wikidata_id,ext_id,latitude,longitude FROM b2q,entry WHERE wikidata_id IN (" . implode(',',array_keys($q_no_coord)) . ") AND building_id=entry.id AND latitude is not null and longitude is not null" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']: '."$sql\n");
	while($o = $result->fetch_object()){
		print "Q" . $o->wikidata_id . "\tP625\t@" . $o->latitude . "/" . $o->longitude . $source ;
	}

}

function createMissingItems ( $data ) {
	global $db , $dbwd ;
	$campaign = $data['campaign'] ;
	$use_id_column = $data['use_id_column'] ;
	$main_prop = $data['prop'] ;
	$type = $data['type'] ;
	$source_item = $data['source_item'] ;
	$has_main_prop = array() ;
	$sql = "SELECT distinct epp_entity_id FROM wb_entity_per_page,pagelinks where pl_from=epp_page_id and epp_entity_type='item' and pl_namespace=120 and pl_title='P$main_prop' and pl_from_namespace=0" ;
	if(!$result = $dbwd->query($sql)) die('There was an error running the query [' . $dbwd->error . ']: '."$sql\n");
	while($o = $result->fetch_object()){
		$has_main_prop[$o->epp_entity_id] = 1 ;
	}

	//print_r ( $has_main_prop ) ; exit(0);

	if ( $source_item == '' ) $source = "\n" ;
	else $source = "\tS143\t$source_item\n" ;
	if ( !isset($data['grades']) ) $data['grades'] = array('');
	foreach ( $data['grades'] AS $grade => $lbq ) {
		$sql = "select distinct $use_id_column,name from entry where campaign='$campaign' AND type='$type' AND NOT EXISTS (select * from b2q where building_id=entry.id) AND do_not_create_wd_item=0" ;
		if ( $grade != '' ) $sql .= " and grade='$grade'" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']: '."$sql\n");
		while($o = $result->fetch_object()){
			print "CREATE\n" ;
			print "LAST\tLen\t\"" . ucwords ( strtolower ( $o->name ) ) . "\"\n" ;
			print "LAST\tP$main_prop\t\"" . $o->$use_id_column . "\"\n" ; # English Heritage list number
			if ( $lbq != '' ) print "LAST\tP31\t$lbq$source" ; # Instance of: Listed building
			print "LAST\tP17\tQ145$source" ; # Country: UK
			print "LAST\tP131\tQ26\n" ; # In admin unit NI
		}
	}
}

/*
function groupApparentIdenticals ( $data ) {
	global $db , $dbwd ;
	$campaign = $data['campaign'] ;
	$use_id_column = $data['use_id_column'] ;
	$main_prop = $data['prop'] ;
	$type = $data['type'] ;
	$source_item = $data['source_item'] ;
	$has_main_prop = array() ;
	
	$idents = array() ;
	$sql = "select name,grade,$use_id_column,type,location,group_concat(id) AS ids,count(*) AS cnt FROM entry WHERE name!='' AND type='$type' AND campaign='$campaign' group by name,grade,$use_id_column,type,location HAVING cnt>1" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']: '."$sql\n");
	while($o = $result->fetch_object()){
		$idents[] = explode ( ',' , $o->ids ) ;
	}

	foreach ( $idents AS $ids ) {
		$sql = "SELECT wikidata_id,count(*) AS cnt FROM b2q WHERE building_id IN (" . implode(',',$ids) . ") GROUP BY wikidata_id" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']: '."$sql\n");
		$qs = array() ;
		$cnt = 0 ;
		while($o = $result->fetch_object()){
			$qs[] = $o->wikidata_id ;
			$cnt = $o->cnt ;
		}
		if ( count($qs) == 0 ) {
//			print "None for $campaign: " . implode(',',$ids) . "\n" ;
			array_shift ( $ids ) ;
			$sql = "UPDATE entry SET do_not_create_wd_item=1 WHERE id IN (" . implode(',',$ids) . ")" ;
			if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']: '."$sql\n");
			continue ;
		}
		if ( count($qs) > 1 ) {
//			print "Multiple for $campaign: " . implode(',',$ids) . "\n" ;
			continue ;
		}
		if ( $cnt == count($ids) ) continue ; // Have them all
		
		$q = $qs[0] ;
		foreach ( $ids AS $id ) {
			$sql = "INSERT IGNORE INTO b2q (building_id,wikidata_id) VALUES ($id,$q)" ;
			if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']: '."$sql\n");
		}
	}
}
*/

foreach ( $sets AS $data ) {
//	groupApparentIdenticals ( $data ) ;
	createMissingItems ( $data ) ;
	createCoordinates ( $data ) ;
}

?>
