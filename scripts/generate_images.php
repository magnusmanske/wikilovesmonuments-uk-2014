#!/usr/bin/php
<?PHP

require_once ( 'php/common.php' ) ;

$db = openToolDB ( 'wlmuk_p' ) ;
$dbwd = openDB ( 'wikidata' , '' ) ;

$q2img = array() ;
$sql = "SELECT wikidata_id AS q,image FROM b2q,existing_media WHERE building_id=entry and image!=''" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']: '."$sql\n");
while($o = $result->fetch_object()){
	$q2img[$o->q] = $o->image ;
}

$sql = "select * from wb_entity_per_page WHERE epp_entity_type='item' and epp_entity_id in (" . implode(',',array_keys($q2img)) . ") AND NOT EXISTS (SELECT * FROM pagelinks WHERE pl_from=epp_page_id AND pl_namespace=120 AND pl_title='P18')" ;
if(!$result = $dbwd->query($sql)) die('There was an error running the query [' . $dbwd->error . ']: '."$sql\n");
while($o = $result->fetch_object()){
	print "Q" . $o->epp_entity_id . "\tP18\t\"" . $q2img[$o->epp_entity_id] . "\"\n" ;
}



/*

$country = 'Scotland' ;
$loc_key = 'parish' ; // community


//$sql = "select distinct location AS loc from entry,region where entry.region=region.id and country='$country' and q is null" ;
$sql = "select distinct parish AS loc from region where country='$country' and q is null" ;
$locations = array() ;
$orig_loc = array() ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']: '."$sql\n");
while($o = $result->fetch_object()){
	$l = $o->loc ;
//	$l = preg_replace ( '/\s+\(.+$/' , '' , $l ) ;
//	$l = preg_replace ( '/^(.+) Boro\b/' , 'Borough of \\1' , $l ) ;
//	$l = preg_replace ( '/ District\b/' , '' , $l ) ;
	$locations[$l] = $db->real_escape_string ( $l ) ;
	$orig_loc[$l] = $o->loc ;
}

//print_r ( $locations ) ;


$l2q = array() ;
//$locations = array('Bracknell Forest'=>'Bracknell Forest') ;
$sql = "SELECT term_entity_id,term_text FROM wb_terms WHERE term_entity_type='item' and term_language='en' and term_type in ('label','alias') and term_text IN ('" . implode("','",$locations) . "')" ;
$sql .= " AND EXISTS (SELECT * FROM wb_entity_per_page,pagelinks WHERE pl_from=epp_page_id AND epp_entity_id=term_entity_id AND epp_entity_type='item' and pl_namespace=0 and pl_title='Q145')" ;
//$sql .= " AND EXISTS (SELECT * FROM wb_entity_per_page,pagelinks WHERE pl_from=epp_page_id AND epp_entity_id=term_entity_id AND epp_entity_type='item' and pl_namespace=120 and pl_title='P131')" ;
//print "$sql\n" ; exit(0) ;

if(!$result = $dbwd->query($sql)) die('There was an error running the query [' . $dbwd->error . ']: '."$sql\n");
while($o = $result->fetch_object()){
	if ( !isset($locations[$o->term_text]) ) {
//		continue ;
	}
	$l2q[$o->term_text][] = $o->term_entity_id ;
//	print $o->term_entity_id . " = " . $o->term_text . "\n" ;
}

//print_r ( $l2q ) ;

foreach ( $l2q AS $l => $q ) {
	if ( count($q) != 1 ) {
		print "SKIPPING $l - multiple targets.\n" ;
		continue ;
	}
	if ( !isset($orig_loc[$l]) ) {
		print "SKIPPING $l - orig loc not found again.\n" ;
		continue ;
	}
	$sql = "UPDATE region SET q=" . $q[0] . " WHERE country='$country' AND $loc_key='" . $db->real_escape_string ( $orig_loc[$l] ) . "'" ;
//	print "$sql;\n" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']: '."$sql\n");
}
*/

?>