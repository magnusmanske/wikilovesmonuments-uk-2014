#!/usr/bin/php
<?PHP

require_once ( 'php/common.php' ) ;

$db = openToolDB ( 'wlmuk_p' ) ;
$dbwd = openDB ( 'wikidata' , '' ) ;

$entry2admin = array() ;
$sql = "select wikidata_id AS q_entry,q AS q_admin from entry,region,b2q WHERE b2q.building_id=entry.id AND region.id=entry.region AND q is not null" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']: '."$sql\n");
while($o = $result->fetch_object()){
	$entry2admin[$o->q_entry] = $o->q_admin ;
}

$sql = "SELECT distinct epp_entity_id FROM wb_entity_per_page WHERE epp_entity_type='item' AND epp_entity_id IN (" . implode(',',array_keys($entry2admin)) . ") AND NOT EXISTS ( SELECT * FROM pagelinks where pl_from=epp_page_id  and pl_namespace=120 and pl_title='P131' and pl_from_namespace=0)" ;
if(!$result = $dbwd->query($sql)) die('There was an error running the query [' . $dbwd->error . ']: '."$sql\n");
while($o = $result->fetch_object()){
	if ( !isset($entry2admin[$o->epp_entity_id]) ) {
		print "?? Q" . $o->epp_entity_id . "\n" ;
		continue ;
	}
	print "Q" . $o->epp_entity_id . "\tP131\tQ" . $entry2admin[$o->epp_entity_id] . "\n" ;
}

?>
