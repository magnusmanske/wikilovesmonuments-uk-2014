#!/usr/bin/php
<?PHP

require_once ( 'php/common.php' ) ;

/*
$campaign = 'nir' ;
$main_prop = 1460 ;
$country = 'Northern Ireland' ;
$use_id_column = 'hbnum' ; // ext_id hbnum
$grade = 'A' ;
$source_item = 'Q6646177' ;
*/


$campaign = 'sct' ;
$main_prop = 709 ;
$country = 'Scotland' ;
$use_id_column = 'hbnum' ; // ext_id hbnum
$grade = 'A,B' ;
$source_item = 'Q111854' ;


/*
$main_prop = 1216 ; // 709
$country = 'England' ; // Scotland
$use_id_column = 'ext_id' ; // ext_id hbnum
$grade = 'I,II*' ; // I,II*
$source_item = 'Q936287' ; // Q111854
*/

$cprop = 625 ;
$url = "$wdq_internal_url?q=claim[$main_prop]&props=$main_prop,625" ; // 
$j = file_get_contents ( $url ) ;
$j = json_decode ( $j ) ;
$j->props->$cprop = array() ;
$p = $main_prop ;
$q2ext = array() ;
foreach ( $j->props->$p AS $v ) $q2ext[$v[0]] = $v[2] ;


$db = openToolDB ( 'wlmuk_p' ) ;
$dbwd = openDB ( 'wikidata' , '' ) ;

$has_main_prop = array() ;
$sql = "SELECT distinct epp_entity_id FROM wb_entity_per_page,pagelinks where pl_from=epp_page_id and epp_entity_type='item' and pl_namespace=120 and pl_title='P$main_prop' and pl_from_namespace=0" ;
if(!$result = $dbwd->query($sql)) die('There was an error running the query [' . $dbwd->error . ']: '."\n$sql\n");
while($o = $result->fetch_object()){
	$has_main_prop[$o->epp_entity_id] = 1 ;
}

$check_q = array() ;

if ( true ) {
	$sql = "SELECT wikidata_id,ext_id FROM b2q,entry WHERE building_id=entry.id AND campaign='$campaign'" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']: '."$sql\n");
	while($o = $result->fetch_object()){
		if ( isset($q2ext[$o->wikidata_id]) ) continue ;
	
		if ( isset ( $has_main_prop[$o->wikidata_id]) ) { // FIX WDQ LAG
			$check_q[] = $o->wikidata_id ;
			continue ;
		}
	
		print "Q" . $o->wikidata_id . "\tP$main_prop\t\"" . $o->ext_id . "\"\n" ;
	
		$q2ext[$o->wikidata_id] = $o->ext_id ;
//		print_r ( $o ) ;
	}
}

// exit ( 0 ) ;


// COORDINATES
//print "!!" . count ( $q2ext ) . "\n" ;
$q_no_coord = array() ;
//$sql = "SELECT distinct epp_entity_id FROM wb_entity_per_page,pagelinks where pl_from=epp_page_id and epp_entity_type='item' and pl_namespace=120 and pl_title='P$main_prop' and pl_from_namespace=0" ;
$sql = "SELECT distinct epp_entity_id FROM wb_entity_per_page WHERE epp_entity_type='item' AND epp_entity_id IN (" . implode(',',array_keys($q2ext)) . ") AND NOT EXISTS ( SELECT * FROM pagelinks where pl_from=epp_page_id  and pl_namespace=120 and pl_title='P625' and pl_from_namespace=0)" ;
if(!$result = $dbwd->query($sql)) die('There was an error running the query [' . $dbwd->error . ']: '."\n$sql\n");
while($o = $result->fetch_object()){
	$q_no_coord[$o->epp_entity_id] = 1 ;
}

$source = "\tS143\t$source_item\n" ;
$sql = "SELECT wikidata_id,ext_id,latitude,longitude FROM b2q,entry WHERE wikidata_id IN (" . implode(',',array_keys($q_no_coord)) . ") AND building_id=entry.id AND latitude is not null and longitude is not null" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']: '."$sql\n");
while($o = $result->fetch_object()){
//	print $o->ext_id . "\n" ;
	print "Q" . $o->wikidata_id . "\tP625\t@" . $o->latitude . "/" . $o->longitude . $source ;
}

?>
